/**
 * Создайте слайс для хранения юзеров
 * При создании обращайте внимание на слайс comments, пусть он будет для вас ориентиром
 * Требования:
 * x 1. в стейте слайса хранится массив юзеров, которых мы когда-то подгружали
 * x 2. в стейте слайса есть объект со статусами загрузок пользователей по ID
 * x 3. в слайсе есть санка на получение пользователя по ID
 * x 4. в слайсе есть редюсер, который обрабатывает санку на получение пользователя по ID
 * x 5. в слайсе есть селектор на получение пользователя по ID из стейта слайса
 * x 6. в слайсе есть селектор на получение статуса загрузки пользователя по ID из стейта
 * x 7. слайс экспортирует своих экшены, селекторы и редюсер из слайса
 */

import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import { RootState } from "../..";
import { getSingleUser } from "../../../api";
import { FetchStatus, User } from "../../../common/types";

const initialState = {
  usersLoadingMap: {} as Record<number, FetchStatus>,
  users: [] as User[],
};

const fetchUserById = createAsyncThunk<User, number>(
  'user/fetchOne',
  async (userId: number): Promise<User> => {
    const user = await getSingleUser(userId);
    return user;
  }
);

export const slice = createSlice({
  name: 'users',
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder.addCase(fetchUserById.pending, (state, { meta }) => {
      state.usersLoadingMap[meta.arg] = 'pending';
    });
    builder.addCase(fetchUserById.rejected, (state, { meta }) => {
      state.usersLoadingMap[meta.arg] = 'rejected';
    });
    builder.addCase(fetchUserById.fulfilled, (state, { meta, payload }) => {
      state.usersLoadingMap[meta.arg] = 'fulfilled';
      state.users = [...state.users, payload];
    })
  }
});

const selectUserById = (userId: number) => (state: RootState) => 
  state.users.users.find((user) => user.id === userId);

const selectUserLoadingStatus = (userId: number) => (state: RootState) =>
  state.users.usersLoadingMap[userId];

export const actions = { ...slice.actions, fetchUserById };
export const reducer = slice.reducer;
export const selectors = {
  selectUserById,
  selectUserLoadingStatus,
};
